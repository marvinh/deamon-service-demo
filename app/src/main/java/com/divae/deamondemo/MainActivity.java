package com.divae.deamondemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.divae.deamonservicelibrary.service.Action;
import com.divae.deamonservicelibrary.service.DeamonService;
import com.divae.deamonservicelibrary.service.Message;
import com.divae.deamonservicelibrary.service.ServiceBinder;
import com.example.mhaagen.daemondemo.R;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String tag = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button start = (Button) findViewById(R.id.start);
        final Button stop = (Button) findViewById(R.id.stop);
        final EditText messageBox = (EditText) findViewById(R.id.messages);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService();
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService();
            }
        });

        DeamonService.registerQueueEventListener(BigInteger.ZERO, new ServiceBinder.OnQueueEventListener() {
            @Override
            public void onNewMessage(List<Message> messages) {
                String text = messageBox.getText().toString();
                for (Message message : messages){
                    text += message.getMessage() + "\n";
                }
                final String textString = text;
                synchronized (messageBox){
                    messageBox.post(new Runnable() {
                        @Override
                        public void run() {
                            messageBox.setText(textString);
                        }
                    });
                }
            }
        });
    }

    private void stopService() {
        Log.d(tag, "Service stopping...");

        //

        DeamonService.stopService(this, BigInteger.ZERO);
    }

    private void startService() {
        Log.d(tag, "Service starting...");

        DeamonService.startService(this, 0, 1000, new MyAction());
    }

    @Override
    protected void onStop() {
        super.onStop();

        DeamonService.saveServiceInfo(this);
    }

    private static class MyAction implements Action {
        @Override
        public void perform(ServiceBinder serviceBinder, String tag) {
            Log.d(tag, String.format("Thread name: %s", Thread.currentThread().getName()));
            serviceBinder.sendMessage(new Message(new Date().toString())); //send current time as an example for communicating with the service
            try {
                Log.d(tag, "Going to sleep...");
                Thread.sleep(10000);
                Log.d(tag, "Waking up...");
            } catch (InterruptedException e) {
                Log.d(tag, "Sleep has been interrupted.");
            }
        }

        @Override
        public BigInteger getID() {
            return BigInteger.ZERO;
        }
    }
}
